import { Hero } from './hero';

export const Heroes: Hero[] = [
    {
        id: 2,
        name: 'Green Lantern'
    },
    {
        id: 3,
        name: 'Captain America'
    },
    {
        id: 4,
        name: 'Superman'
    },
    {
        id: 5,
        name: 'Iron Man'
    },
    {
        id: 6,
        name: 'Hulk'
    },
    {
        id: 7,
        name: 'Deadpool'
    },
    {
        id: 8,
        name: 'Static Shock'
    },
    {
        id: 9,
        name: 'Black Panther'
    },
    {
        id: 10,
        name: 'Aqua-man'
    }
]